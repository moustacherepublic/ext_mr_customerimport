<?php
class MR_Customerimport_ImportController extends Mage_Core_Controller_Front_Action{
    public function importCustomerAction(){
        $maxTime = ini_get('max_execution_time');
        set_time_limit(0);//0 means unlimited
        $response = array("error" => false);
        $importModel = Mage::getModel('mr_customerimport/import');
        try {
            $importModel->importCustomer();
        }
        catch (Exception $e) {
            Mage::logException($e);
            $response['error'] = $e->getMessage();
            $importModel->unlockImport();
        }
        set_time_limit($maxTime);
        $this->getResponse()
            ->setHeader("Content-Type", "application/json")
            ->setBody(json_encode($response));

    }
}