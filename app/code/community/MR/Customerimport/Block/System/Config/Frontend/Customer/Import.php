<?php
/**
 * Copyright (c) 2013 S.L.I. Systems, Inc. (www.sli-systems.com) - All Rights Reserved
 * This file is part of Learning Search Connect.
 * Learning Search Connect is distribute under license,
 * go to www.sli-systems.com/LSC for full license details.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * Source renderer for product attributes data.
 *
 * @package SLI
 * @subpackage Search
 */

class MR_Customerimport_Block_System_Config_Frontend_Customer_Import extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected $_buttonId = "mr_customerimport_button";

    /**
     * Programmatically include the generate feed javascript in the adminhtml
     * JS block.
     *
     * @return <type>
     */
    protected function _prepareLayout() {
        $block = $this->getLayout()->createBlock("mr_customerimport/system_config_frontend_customer_import_js");
        $block->setData("button_id", $this->_buttonId);

        $this->getLayout()->getBlock('js')->append($block);
        return parent::_prepareLayout();
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $button = $this->getButtonHtml();

        $notice = "";
        if ($this->_customerImportIsLocked()) {
            $notice = "<p id='mr_customerimport_msg' class='note'>".Mage::getModel("mr_customerimport/import")->getAjaxNotice()."</p>";
        }
        return $button.$notice;
    }

    /**
     * Generate button html for the feed button
     *
     * @return string
     */
    public function getButtonHtml() {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'id' => $this->_buttonId,
                'label' => $this->helper('mr_customerimport')->__('Import Customers'),
                'onclick' => 'javascript:mrCustomerimport.importCustomer(); return false;'
            ));

        if ($this->_customerImportIsLocked()) {
            $button->setData('class', 'disabled');
        }

        return $button->toHtml();
    }

    /**
     * Check to see if there are any locks for any feeds at the moment
     *
     * @return boolean
     */
    protected function _customerImportIsLocked() {
        return Mage::helper('mr_customerimport')->thereAreImportLocks();
    }

}
