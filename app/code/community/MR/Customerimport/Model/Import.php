<?php
class MR_Customerimport_Model_Import extends Mage_Core_Model_Abstract{
    public  $_ajaxNotice = "Currently importing customers...";
    protected $_importLockFile = null;

    public function importCustomer(){
        $helper = Mage::helper('mr_customerimport');
        if($helper->thereAreImportLocks()){
            throw new Exception("Customer importing. Customer import temporarily locked.");
        }

        $this->_feedLockFile = null;
        $this->_lockImport();

        $customerDir = $helper->getCustomerFilePath();
        $customerFiles = scandir($customerDir);
        foreach ($customerFiles as $file) {
            if($file == '.' || $file == '..' || is_numeric(strpos($file, '.lock'))){
                continue;
            }
            $csvObject = new Varien_File_Csv();
            $csvData = $csvObject->getData($this->_getFile($file));
            // print_r($csvData);die;
            foreach($csvData as $key => $value){
                if($key != 0){
                    $email = $value[3];
                    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
                    if(!$email){
                        continue;
                    }
                    $customer = Mage::getModel('customer/customer');
                    $customer->setWebsiteId(1);//NZ
                    $customer->loadByEmail($email);
                    if($customer->getId()){
                        continue;
                    }
                    $password = $value[1];
                    if(strlen($password)<6){
                        $password = $customer->generatePassword();
                    }
                    $phone = $value[2];
                    $company = $value[15];
                    $firstname = $value[16] ? : 'firstname';
                    $lastname = $value[17] ? : 'lastname';
                    $billing_address_street1 = $value[18];
                    $billing_address_street2 = $value[19];
                    $city = $value[21];
                    $region = $value[22];
                    $postcode = $value[23];
                    $country = trim($value[24]);
                    $country_id = $this->_getCountryId($country);

                    $shipping_firstname = $value[26];
                    $shipping_lastname = $value[27];
                    $shipping_company = $value[25] ? $value[25] : $company;
                    $shipping_address_street1 = $value[28] ? $value[28] : $billing_address_street1;
                    $shipping_address_street2 = $value[29] ? $value[29] : $billing_address_street2;
                    $shipping_city = $value[31] ? $value[31] : $city;
                    $shipping_region = $value[32] ? $value[32] : $region;
                    $shipping_postcode = $value[33] ? $value[33] : $postcode;
                    $shipping_country = trim($value[34]) ? trim($value[34]) : $country;
                    $shipping_country_id = $this->_getCountryId($shipping_country);

                    //start to import

                    $customer->setEmail($email);
                    $customer->setFirstname($firstname);
                    $customer->setLastname($lastname);
                    $customer->setPassword($password);
                    try{
                        $customer->save();
//                        $customer->setConfirmation(null);
//                        $customer->save();
                    }
                    catch(Exception $e){
                        Mage::log('customer ' . $email . ' save:' . $e->getMessage(), null , 'customer_import.log' , true);
                    }

                    $customer_billing_address = array(
                        'firstname' => $firstname,
                        'lastname' => $lastname,
                        'company' => $company,
                        'street' => array(
                            '0' => $billing_address_street1,
                            '1' => $billing_address_street2
                        ),
                        'city' => $city,
                        'region' => $region,
                        'country_id' => $country_id,
                        'postcode' => $postcode,
                        'telephone' => $phone
                    );

                    $customerAddress = Mage::getModel('customer/address');
                    $customerAddress->setData($customer_billing_address)
                        ->setCustomerId($customer->getId())
                        ->setIsDefaultBilling('1');
                    if(empty($shipping_firstname)){
                        $customerAddress->setIsDefaultShipping('1');
                    }
                    $customerAddress->setSaveInAddressBook('1');
                    try{
                        $customerAddress->save();
                    }
                    catch(Exception $e){
                        Mage::log('customer ' . $email . ' billing address save:' . $e->getMessage(), null , 'customer_import.log' , true);
                    }

                    if(!empty($shipping_firstname)){
                        $customer_shipping_address = array(
                            'firstname' => $shipping_firstname,
                            'lastname' => $shipping_lastname,
                            'company' => $shipping_company,
                            'street' => array(
                                '0' => $shipping_address_street1,
                                '1' => $shipping_address_street2
                            ),
                            'postcode' => $shipping_postcode,
                            'city' => $shipping_city,
                            'region' => $shipping_region,
                            'country_id' => $shipping_country_id,
                            'telephone' => $phone
                        );
                        $shipping_address = Mage::getModel('customer/address');
                        $shipping_address->setData($customer_shipping_address)
                            ->setCustomerId($customer->getId())
                            ->setIsDefaultShipping('1')
                            ->setSaveInAddressBook('1');
                        try{
                            $shipping_address->save();
                        }
                        catch(Exception $e){
                            Mage::log('customer ' . $email . ' shipping address save:' . $e->getMessage(), null , 'customer_import.log' , true);
                        }
                    }
                    Mage::log($file . ':' . $key, null , 'customer_import_process.log' , true);
                }
            }
        }
        $this->unlockImport();
    }

    protected function _getFile($filename){
        return Mage::getBaseDir() . DS . 'var' . DS . 'import' . DS . 'customer' . DS . $filename;
    }

    protected function _getImportLockFile() {
        if ($this->_importLockFile === null) {
            $importDir =  Mage::helper('mr_customerimport')->getCustomerFilePath();
            $this->_importLockFile = $importDir . DS . "customerimport.lock";
        }
        return $this->_importLockFile;
    }


    protected function _lockImport() {
        file_put_contents($this->_getImportLockFile(), 1);
    }

    public function unlockImport(){
        $lockFile = $this->_getImportLockFile();
        if (is_file($lockFile)) {
            unlink($lockFile);
        }
    }

    protected function _getCountryId($country){
        $country_id = '';
        switch ($country) {
            case 'NZ':
            case 'New Zealan':
                $country_id = 'NZ';
                break;
            case 'AU':
            case 'Australia':
                $country_id = 'AU';
                break;
            case 'UK':
            case 'United Kin':
                $country_id = 'GB';
                break;
            case 'US':
            case 'United Sta':
                $country_id = 'US';
                break;
            case 'Germany':
                $country_id = 'DE';
                break;
            default:
                $country_id = 'NZ';
                break;
        }
//        Mage::log($country_id, null , 'customer_import_process.log' , true);
        return $country_id;
    }

    public function getAjaxNotice() {
        return $this->_ajaxNotice;
    }
}