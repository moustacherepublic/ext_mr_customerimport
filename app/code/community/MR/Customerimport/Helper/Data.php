<?php
class MR_Customerimport_Helper_Data extends Mage_Core_Helper_Abstract {
    protected $_customerFilePath = null;

    public function getCustomerFilePath(){
        if ($this->_customerFilePath === null) {
            $this->_customerFilePath = Mage::getBaseDir() . DS . 'var' . DS . 'import' . DS . 'customer';
        }

        return $this->_customerFilePath;
    }

    /**
     * Whether or not there are customer import locks currently in place
     *
     * @return boolean
     */
    public function thereAreImportLocks() {
        $path = $this->getCustomerFilePath();
        foreach (scandir($path) as $file) {
            $fullFile = $path.DS.$file;
            if (is_file($fullFile) && !is_dir($fullFile) && is_numeric(strpos($file, '.lock'))) {
                return true;
            }
        }
        return false;
    }
}